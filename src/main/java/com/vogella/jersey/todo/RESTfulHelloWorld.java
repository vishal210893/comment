package com.vogella.jersey.todo;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import FileContainer.Readfromfile;
import FileContainer.Writetofile;

@Path("/")
public class RESTfulHelloWorld {

	@GET
	@Path("comment/{id}")
	@Produces("text/html")
	public Response getStartingPage(@PathParam("id") String id) {
		System.out.println("id is " + id);
		int value = Readfromfile.readFromFile(id);
		String output = "<h1>No. of Comment for post with id " + id
				+ "<h1>is <br>" + value
				+ "<br>";
		return Response.status(200).entity(output).build();
	}


	@POST
	@Path("comment/post/{id}/{post}")
	@Produces("application/json")
	@Consumes("application/json")
	public Response writeDataToFile(@PathParam("id") String id,@PathParam("post") String post) {
		System.out.println("Coming inside this method");
		String result = "";
		boolean ret = Writetofile.writeToFile(id,post);
		if (ret) {
			result = "Posted Successfully";
		} else {
			result = "Sorry there is an error";
		}
		return Response.status(201).entity(result).build();

	}

}
