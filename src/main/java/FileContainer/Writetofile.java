package FileContainer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Writetofile {

	private static final String FILENAME = "C:\\Users\\Vishal Kumar\\eclipse-workspace\\Comment.txt";

	public static boolean writeToFile(String id,String post) {

		BufferedWriter bw = null;
		FileWriter fw = null;
		try {	
				fw = new FileWriter(FILENAME,true);
				bw = new BufferedWriter(fw);
				bw.write(id + "        " + post);
				bw.newLine();
			}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return true;
	}
}